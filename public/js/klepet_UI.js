/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}

function vrniLinkSlike(besedilo) {
  var tabela = besedilo.replace('"','').split(' ');
  console.log(tabela);
  console.log(tabela.length);
  var regexLavbic = /(sandbox.lavbic.net\S)/g;
  var link = "neki";
  console.log("Check na beseilo: "+(regexSlika.test(besedilo) && !regexLavbic.test(besedilo)));
  for(var i = 0; i < tabela.length; i++) {
    
    var pogoj = (regexSlika.test(tabela[i]) || (tabela[i].includes('http') && (tabela[i].includes('.jpg') || tabela[i].includes('.gif') || tabela[i].includes('.png'))) && !(regexLavbic.test(tabela[i])) && !tabela[i].includes('src='));
    console.log(pogoj);
    if(pogoj) {
      console.log("Najden link.");
      return tabela[i];
    } else {
      console.log(pogoj);
    }
  }
  
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

var regexSlika = /(http\S+(?:jpg|png|gif))/g;

/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sporociloPrvotno = sporocilo;
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  }
  // Ce je v sporocilu slika
  else if(regexSlika.test(sporociloPrvotno) || (sporociloPrvotno.includes('http') && (sporociloPrvotno.includes('.jpg') || sporociloPrvotno.includes('.gif') || sporociloPrvotno.includes('.png')))) {
    var link2 = vrniLinkSlike(sporociloPrvotno);
    
    var slika = $('<img style="width:200px; margin-left:20px;" src="'+link2+'"/></br>');
    var sporociloSlika = $('<div style="font-weight: bold"></div>').text($('#poslji-sporocilo').val());
    $('#sporocila').append(divElementEnostavniTekst(sporocilo)).append(slika);
    klepetApp.posljiSporocilo(trenutniKanal,sporociloPrvotno);
  
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";
var nadimena = [];

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var testNoRegex = (!sporocilo.besedilo.includes('src=') && sporocilo.besedilo.includes('http') && (sporocilo.besedilo.includes('.jpg') || sporocilo.besedilo.includes('.gif') || sporocilo.besedilo.includes('.png')));
    var primerenNadimek = "";
    
    if(sporocilo.besedilo.includes('zasebno')) {
      var imeZasebno = sporocilo.besedilo.split(' ')[0];
      var besediloZasebno = sporocilo.besedilo.split(':')[1];
      for(var i = 0; i <nadimena.length; i++) {
        if(nadimena[i].uporabnik == imeZasebno) {
          primerenNadimek = nadimena[i].nadimek;
          break;
          
        }
      }
      
      if(primerenNadimek != "")
        var dodanNadimekBesedilo = primerenNadimek+"("+imeZasebno+")"+" (zasebno)"+": "+besediloZasebno;
      else
        var dodanNadimekBesedilo = sporocilo.besedilo;
      
    } else {
      var sporociloSplit = sporocilo.besedilo.split(':');
      var ime = sporocilo.besedilo.split(':')[0];
      var besedilo = sporocilo.besedilo.substring(sporocilo.besedilo.indexOf(':')+1);
      for(var i = 2; i < sporociloSplit.length; i++) {
        besedilo = besedilo + sporociloSplit[i];
      }
      
      
      for(var i = 0; i <nadimena.length; i++) {
        if(nadimena[i].uporabnik == ime) {
          primerenNadimek = nadimena[i].nadimek;
          break;
          
        }
      }
      
      if(primerenNadimek != "")
        var dodanNadimekBesedilo = primerenNadimek+"("+ime+")"+":"+besedilo;
      else
        var dodanNadimekBesedilo = sporocilo.besedilo;
    }
    
    
    // Krcanje
    if(sporocilo.besedilo.includes('&#9756;')) {
      var message = divElementEnostavniTekst(dodanNadimekBesedilo.replace('&#9756;','')+'☜');
      $('#sporocila').append(message);
    } 
    // V primeru da je poslana slika
    else if(regexSlika.test(sporocilo.besedilo) || testNoRegex ) {
      console.log("prejemanje sporocila :"+sporocilo.besedilo);
      var link3 = vrniLinkSlike(sporocilo.besedilo);
      console.log("Prejemanje sporocila link: "+link3);
      
      
      var slikaKoncna = $('<img style="width:200px; margin-left:20px;" src="'+link3+'"/></br>');
     
      if(slikaKoncna)
        $('#sporocila').append(divElementEnostavniTekst(dodajSmeske(dodanNadimekBesedilo))).append(slikaKoncna);
        
    } else {
      
      var novElement = divElementEnostavniTekst(dodanNadimekBesedilo);
      $('#sporocila').append(novElement);
    }

    

  });
  
  
  //Procesiraj spremembo barve kanala
  socket.on('spremembaBarveOdgovor',function(barva) {
    
    $('#kanal')
    .css('background-color', '')
    .css('background-color', barva.barva);
    
    $('#sporocila')
    .css('background-color', '')
    .css('background-color', barva.barva);
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  

  // OBDELAVA ZA DODAJANJE PRIDRUZITVI KANALA

  socket.on('spremembaVzdevkaNadimek',function(podatki) {
    console.log(podatki);
    var novVzdevek = podatki.novVzdevek;
    var stariVzdevek = podatki.prejsniVzdevek;
    for(var i = 0; i < nadimena.length; i++) {
      if(nadimena[i].uporabnik == stariVzdevek) {
        nadimena[i].uporabnik = novVzdevek;
        console.log(nadimena[i]);
        break;
      }
    }
    console.log(nadimena);
  });
  
  
  // Obdelava preimenovanja uporabnika
  socket.on('preimenujOdgovor',function(podatki) {
    var jeZeNotri = false;
    for(var i = 0; i < nadimena.length; i++) {
      if(nadimena[i].uporabnik == podatki.uporabnik) {
        nadimena[i].nadimek = podatki.nadimek;
        jeZeNotri = true;
      }
    }
    if(!jeZeNotri)
      nadimena.push(podatki);
    
    console.log(nadimena);
    console.log(podatki.uporabnik);
    console.log(podatki.nadimek);
  });
  

  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    
    for (var i=0; i < uporabniki.length; i++) {
      var trenutnoNadime = "";
      for(var j = 0; j < nadimena.length; j++) {
        if(nadimena[j].uporabnik == uporabniki[i]) {
          trenutnoNadime = nadimena[j].nadimek;
        }
      }
      if(trenutnoNadime != "") {
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(trenutnoNadime+"("+uporabniki[i]+")").click(function() {
        var ime = $(this).text();
        if(ime.includes('(') && ime.includes(')'))
          ime = ime.replace('(',' ').replace(')',' ').split(' ')[1];
        console.log(ime);
        var odgovor = klepetApp.procesirajUkaz('/krcanje ' + ime);
        if (odgovor) {
          $('#sporocila').append(divElementHtmlTekst(odgovor));
          }
        }));
      } else {
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]).click(function() {
        var ime = $(this).text();
        var odgovor = klepetApp.procesirajUkaz('/krcanje ' + ime);
        if (odgovor) {
          $('#sporocila').append(divElementHtmlTekst(odgovor));
          }
        }));
      }
      

    }
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
